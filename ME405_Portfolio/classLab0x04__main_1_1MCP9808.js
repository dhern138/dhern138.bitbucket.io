var classLab0x04__main_1_1MCP9808 =
[
    [ "__init__", "classLab0x04__main_1_1MCP9808.html#aa1e32df13ed229906c6ef5a85463973c", null ],
    [ "celcius", "classLab0x04__main_1_1MCP9808.html#addb5bf5cfd2696a64f8a6081b8162e41", null ],
    [ "check", "classLab0x04__main_1_1MCP9808.html#ac2e3195e5b3927e63bd0054c50905758", null ],
    [ "temp_F", "classLab0x04__main_1_1MCP9808.html#adc7e5f1e2dd267ebd4cd1d98dad988f9", null ],
    [ "Dev_addr", "classLab0x04__main_1_1MCP9808.html#afa58220db91e2fae729dff644668f7b0", null ],
    [ "I2C_bus", "classLab0x04__main_1_1MCP9808.html#a9344b1f69f73f0b20c117607d4eb641f", null ],
    [ "ID", "classLab0x04__main_1_1MCP9808.html#a5892010c8f104f8c20487f1f5368cffe", null ],
    [ "mfg_reg", "classLab0x04__main_1_1MCP9808.html#ac4e4a5fd3f35ad8e13dc9f0744f9fff3", null ],
    [ "registers", "classLab0x04__main_1_1MCP9808.html#adcf662b4684f07caf68770d56aaf38da", null ],
    [ "temp_C", "classLab0x04__main_1_1MCP9808.html#a4e0161df4e345995382a4c850a3d0765", null ]
];