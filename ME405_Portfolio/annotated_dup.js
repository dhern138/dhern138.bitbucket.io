var annotated_dup =
[
    [ "closedLoop", null, [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "encoderDriver", null, [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "Lab0x04_main", null, [
      [ "MCP9808", "classLab0x04__main_1_1MCP9808.html", "classLab0x04__main_1_1MCP9808" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ]
];