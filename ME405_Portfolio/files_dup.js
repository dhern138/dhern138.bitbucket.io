var files_dup =
[
    [ "backEndTask.py", "backEndTask_8py.html", "backEndTask_8py" ],
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "controllerTask.py", "controllerTask_8py.html", "controllerTask_8py" ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "dataCollectTask.py", "dataCollectTask_8py.html", "dataCollectTask_8py" ],
    [ "encoderDriver.py", "encoderDriver_8py.html", "encoderDriver_8py" ],
    [ "encoderTask.py", "encoderTask_8py.html", "encoderTask_8py" ],
    [ "Lab0x02A.py", "Lab0x02A_8py.html", "Lab0x02A_8py" ],
    [ "Lab0x02B.py", "Lab0x02B_8py.html", "Lab0x02B_8py" ],
    [ "Lab0x03_main.py", "Lab0x03__main_8py.html", "Lab0x03__main_8py" ],
    [ "Lab0x03_UI.py", "Lab0x03__UI_8py.html", "Lab0x03__UI_8py" ],
    [ "Lab0x04_main.py", "Lab0x04__main_8py.html", "Lab0x04__main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorTask.py", "MotorTask_8py.html", "MotorTask_8py" ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "refund.py", "refund_8py.html", "refund_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "TouchDriver.py", "TouchDriver_8py.html", "TouchDriver_8py" ],
    [ "TouchPanelTask.py", "TouchPanelTask_8py.html", "TouchPanelTask_8py" ],
    [ "UI_front.py", "UI__front_8py.html", "UI__front_8py" ],
    [ "vendorLab0x01.py", "vendorLab0x01_8py.html", "vendorLab0x01_8py" ]
];