var TouchDriver_8py =
[
    [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ],
    [ "PIN_xm", "TouchDriver_8py.html#a0aafc01a19eec0baab5a56e8e5c34896", null ],
    [ "PIN_xp", "TouchDriver_8py.html#a4e1a353719dada5e6bb13e6cb481f176", null ],
    [ "PIN_ym", "TouchDriver_8py.html#a2378adb5307f7346aa5e8f1ab33982bc", null ],
    [ "PIN_yp", "TouchDriver_8py.html#add184b246838e14a526692fa1b3495ec", null ],
    [ "pos", "TouchDriver_8py.html#ac4b08bf5c1c03afce2e884df50acd64b", null ],
    [ "run_time", "TouchDriver_8py.html#a36eb81da32fa799b3594d29f7ee06938", null ],
    [ "sel_axis", "TouchDriver_8py.html#a4c53f9750f750bec4a91f9e6422a351b", null ],
    [ "sel_test", "TouchDriver_8py.html#ae2eac6d2611255cccc00181cda5c4a85", null ],
    [ "start", "TouchDriver_8py.html#a8b05ca676f0a3deb2549c1eed9f0a789", null ],
    [ "stop", "TouchDriver_8py.html#afb89a36463df1e2b0a4dba08116d9c12", null ],
    [ "TouchPanel", "TouchDriver_8py.html#ad105ffbd9d3de069b2dbf1c087abda51", null ],
    [ "x", "TouchDriver_8py.html#a0b46735b2a08a636bb5378142f302c98", null ],
    [ "y", "TouchDriver_8py.html#acdacbd83d4bdf237e411f3a6388fbaaf", null ],
    [ "z", "TouchDriver_8py.html#a4f219e389d4ece1d46264e5e7d44f4c1", null ]
];