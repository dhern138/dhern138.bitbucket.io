var shares_8py =
[
    [ "alpha_gain", "shares_8py.html#af74423c00aac51e9048b062aac662ce9", null ],
    [ "BACKstate", "shares_8py.html#aec4153ce0a80e176d50349d9ec2ee4ee", null ],
    [ "beta_gain", "shares_8py.html#a309d0ea046d5bbb9c64532220a3463c3", null ],
    [ "encoder2Position", "shares_8py.html#ac1e0b39e80518644cc8127b66ac42e20", null ],
    [ "encoderPosition", "shares_8py.html#adee4a6ca231638a930a1cc841b3178a9", null ],
    [ "ENCODERstate", "shares_8py.html#abbf96cfe710c8d8331b50e6e9f9c07be", null ],
    [ "pos1Ref", "shares_8py.html#ab7cccdff76430d7da8ae41434c587c87", null ],
    [ "pos2Ref", "shares_8py.html#a1627d4a08ff5c52f2dde2ff6970f356c", null ],
    [ "posMeas", "shares_8py.html#aaac4cef55a02d9972651a7bdf71720ad", null ],
    [ "PWM1", "shares_8py.html#a17ae7220cac4183b25d46b57b80a077b", null ],
    [ "PWM2", "shares_8py.html#a781500ea75dc4796c30c49e8465f4447", null ],
    [ "speed1Meas", "shares_8py.html#ad222a2ab5d42683f9f3f345690f79884", null ],
    [ "speed2Meas", "shares_8py.html#ac4f6f801c42c3262b6d70b47ecc5af95", null ],
    [ "speedRef", "shares_8py.html#ae53517fcf4463ec229ed9638d71c6b3d", null ],
    [ "tref1", "shares_8py.html#a28e9eafe3dacfb649abe881834a5a60f", null ],
    [ "tref2", "shares_8py.html#a37bc2e506c28a8782223f93adc7161dd", null ],
    [ "v_x", "shares_8py.html#a6711501dd96ee8d8588ae0c0d379213c", null ],
    [ "v_y", "shares_8py.html#a945c72d66896246b2dbb3c8533f63a64", null ],
    [ "x", "shares_8py.html#aa3104f8fdb6091da6ec17acaa5fe0087", null ],
    [ "y", "shares_8py.html#a29c058d83fed912720d2cc7e93ed8b43", null ]
];