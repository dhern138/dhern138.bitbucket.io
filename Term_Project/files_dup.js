var files_dup =
[
    [ "closedLoop_part3.py", "closedLoop__part3_8py.html", [
      [ "closedLoop_part3", "classclosedLoop__part3_1_1closedLoop__part3.html", "classclosedLoop__part3_1_1closedLoop__part3" ]
    ] ],
    [ "closedLoop_part4.py", "closedLoop__part4_8py.html", [
      [ "closedLoop_part4", "classclosedLoop__part4_1_1closedLoop__part4.html", "classclosedLoop__part4_1_1closedLoop__part4" ]
    ] ],
    [ "controllerTask_part3.py", "controllerTask__part3_8py.html", [
      [ "controllerTask_part3", "classcontrollerTask__part3_1_1controllerTask__part3.html", "classcontrollerTask__part3_1_1controllerTask__part3" ]
    ] ],
    [ "controllerTask_part4.py", "controllerTask__part4_8py.html", [
      [ "controllerTask_part4", "classcontrollerTask__part4_1_1controllerTask__part4.html", "classcontrollerTask__part4_1_1controllerTask__part4" ]
    ] ],
    [ "encoderDriver_part2.py", "encoderDriver__part2_8py.html", [
      [ "encoderDriver_part2", "classencoderDriver__part2_1_1encoderDriver__part2.html", "classencoderDriver__part2_1_1encoderDriver__part2" ]
    ] ],
    [ "encoderDriver_part3.py", "encoderDriver__part3_8py.html", [
      [ "encoderDriver_part3", "classencoderDriver__part3_1_1encoderDriver__part3.html", "classencoderDriver__part3_1_1encoderDriver__part3" ]
    ] ],
    [ "encoderDriver_part4.py", "encoderDriver__part4_8py.html", [
      [ "encoderDriver_part4", "classencoderDriver__part4_1_1encoderDriver__part4.html", "classencoderDriver__part4_1_1encoderDriver__part4" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", [
      [ "encoderTask", "classencoderTask_1_1encoderTask.html", "classencoderTask_1_1encoderTask" ]
    ] ],
    [ "main_part2.py", "main__part2_8py.html", "main__part2_8py" ],
    [ "main_part3.py", "main__part3_8py.html", "main__part3_8py" ],
    [ "main_part4.py", "main__part4_8py.html", "main__part4_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "UI_backTask_part1.py", "UI__backTask__part1_8py.html", "UI__backTask__part1_8py" ],
    [ "UI_backTask_part2.py", "UI__backTask__part2_8py.html", [
      [ "UI_backTask_part2", "classUI__backTask__part2_1_1UI__backTask__part2.html", "classUI__backTask__part2_1_1UI__backTask__part2" ]
    ] ],
    [ "UI_backTask_part3.py", "UI__backTask__part3_8py.html", [
      [ "UI_backTask_part3", "classUI__backTask__part3_1_1UI__backTask__part3.html", "classUI__backTask__part3_1_1UI__backTask__part3" ]
    ] ],
    [ "UI_backTask_part4.py", "UI__backTask__part4_8py.html", [
      [ "UI_backTask_part4", "classUI__backTask__part4_1_1UI__backTask__part4.html", "classUI__backTask__part4_1_1UI__backTask__part4" ]
    ] ],
    [ "UI_front_part1.py", "UI__front__part1_8py.html", "UI__front__part1_8py" ],
    [ "UI_front_part2.py", "UI__front__part2_8py.html", "UI__front__part2_8py" ],
    [ "UI_front_part3.py", "UI__front__part3_8py.html", "UI__front__part3_8py" ],
    [ "UI_front_part4.py", "UI__front__part4_8py.html", "UI__front__part4_8py" ]
];