var classclosedLoop__part4_1_1closedLoop =
[
    [ "__init__", "classclosedLoop__part4_1_1closedLoop.html#a34682068d0a6bc73c8dc1fd244f45164", null ],
    [ "get_Kp", "classclosedLoop__part4_1_1closedLoop.html#a6857cceff5c3bd767926e64bf8645062", null ],
    [ "set_Ki", "classclosedLoop__part4_1_1closedLoop.html#aec43138590d1f4996bded85fbc46856a", null ],
    [ "set_Kp", "classclosedLoop__part4_1_1closedLoop.html#a513ad2ba265133ac70986f8301bfb4d8", null ],
    [ "update", "classclosedLoop__part4_1_1closedLoop.html#a4e5bed4911340eb1cd379c9a4d25157f", null ],
    [ "Ki", "classclosedLoop__part4_1_1closedLoop.html#af234b3f779dc29d510dc48c0ef4591ae", null ],
    [ "Kp", "classclosedLoop__part4_1_1closedLoop.html#a5d968ac82fe876a4e4e54f2f17c13435", null ],
    [ "Pmax", "classclosedLoop__part4_1_1closedLoop.html#a2b6e8dfc520b12e7d6f21037ff91b585", null ],
    [ "Pmin", "classclosedLoop__part4_1_1closedLoop.html#a009286d1e1eaed70ad92297b166536cc", null ],
    [ "PWM", "classclosedLoop__part4_1_1closedLoop.html#a8f4af0a94e384e16dec32dc3ebe392a6", null ],
    [ "PWMdelta", "classclosedLoop__part4_1_1closedLoop.html#acdd3cc94f16bc5627a2b3e0314563526", null ]
];