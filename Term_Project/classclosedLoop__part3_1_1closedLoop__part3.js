var classclosedLoop__part3_1_1closedLoop__part3 =
[
    [ "__init__", "classclosedLoop__part3_1_1closedLoop__part3.html#a743a04dd687b486e207adbe0e26320c1", null ],
    [ "get_Kp", "classclosedLoop__part3_1_1closedLoop__part3.html#a1aa62cea7aa8c0dbaf36a6a915756260", null ],
    [ "set_Kp", "classclosedLoop__part3_1_1closedLoop__part3.html#a02bc034cc703750e2d06e6e5edd3a072", null ],
    [ "update", "classclosedLoop__part3_1_1closedLoop__part3.html#a0b42af13302f102b64158785f27636b2", null ],
    [ "Ki", "classclosedLoop__part3_1_1closedLoop__part3.html#af3a35754e9ddb5bd6196e91fee3a2a07", null ],
    [ "Kp", "classclosedLoop__part3_1_1closedLoop__part3.html#ab9764c0a9a50008b3388c78a3a623434", null ],
    [ "Pmax", "classclosedLoop__part3_1_1closedLoop__part3.html#a8d23394bc0d9855ea060d6bde6d5da27", null ],
    [ "Pmin", "classclosedLoop__part3_1_1closedLoop__part3.html#ad9a8ba3cbb1dc9e5ef128a8503e454b2", null ],
    [ "PWM", "classclosedLoop__part3_1_1closedLoop__part3.html#a1799453b967a229992f5591344c130c0", null ],
    [ "PWMdelta", "classclosedLoop__part3_1_1closedLoop__part3.html#a7869d257cf6cef883412786e6e2bc469", null ]
];