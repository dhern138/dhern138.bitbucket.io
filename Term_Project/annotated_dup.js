var annotated_dup =
[
    [ "closedLoop_part3", null, [
      [ "closedLoop_part3", "classclosedLoop__part3_1_1closedLoop__part3.html", "classclosedLoop__part3_1_1closedLoop__part3" ]
    ] ],
    [ "closedLoop_part4", null, [
      [ "closedLoop_part4", "classclosedLoop__part4_1_1closedLoop__part4.html", "classclosedLoop__part4_1_1closedLoop__part4" ]
    ] ],
    [ "controllerTask_part3", null, [
      [ "controllerTask_part3", "classcontrollerTask__part3_1_1controllerTask__part3.html", "classcontrollerTask__part3_1_1controllerTask__part3" ]
    ] ],
    [ "controllerTask_part4", null, [
      [ "controllerTask_part4", "classcontrollerTask__part4_1_1controllerTask__part4.html", "classcontrollerTask__part4_1_1controllerTask__part4" ]
    ] ],
    [ "encoderDriver_part2", null, [
      [ "encoderDriver_part2", "classencoderDriver__part2_1_1encoderDriver__part2.html", "classencoderDriver__part2_1_1encoderDriver__part2" ]
    ] ],
    [ "encoderDriver_part3", null, [
      [ "encoderDriver_part3", "classencoderDriver__part3_1_1encoderDriver__part3.html", "classencoderDriver__part3_1_1encoderDriver__part3" ]
    ] ],
    [ "encoderDriver_part4", null, [
      [ "encoderDriver_part4", "classencoderDriver__part4_1_1encoderDriver__part4.html", "classencoderDriver__part4_1_1encoderDriver__part4" ]
    ] ],
    [ "encoderTask", null, [
      [ "encoderTask", "classencoderTask_1_1encoderTask.html", "classencoderTask_1_1encoderTask" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "UI_backTask_part2", null, [
      [ "UI_backTask_part2", "classUI__backTask__part2_1_1UI__backTask__part2.html", "classUI__backTask__part2_1_1UI__backTask__part2" ]
    ] ],
    [ "UI_backTask_part3", null, [
      [ "UI_backTask_part3", "classUI__backTask__part3_1_1UI__backTask__part3.html", "classUI__backTask__part3_1_1UI__backTask__part3" ]
    ] ],
    [ "UI_backTask_part4", null, [
      [ "UI_backTask_part4", "classUI__backTask__part4_1_1UI__backTask__part4.html", "classUI__backTask__part4_1_1UI__backTask__part4" ]
    ] ]
];