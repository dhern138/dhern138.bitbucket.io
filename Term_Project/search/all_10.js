var searchData=
[
  ['s0_5finit_87',['S0_INIT',['../classUI__backTask__part3_1_1UI__backTask__part3.html#aed6f6ec7c91cba2c9348fa799e3974f2',1,'UI_backTask_part3.UI_backTask_part3.S0_INIT()'],['../classUI__backTask__part4_1_1UI__backTask__part4.html#a2a2168bab8008dc69f4e8732bd9f3368',1,'UI_backTask_part4.UI_backTask_part4.S0_INIT()']]],
  ['scsv_88',['scsv',['../shares_8py.html#a5895782b1b724363e30d444fe9b58ce5',1,'shares']]],
  ['ser_89',['ser',['../UI__front__part1_8py.html#ab70d2546f02e60ec41d30a2eed396e9e',1,'UI_front_part1.ser()'],['../UI__front__part2_8py.html#aca0168b17a8908a0724fbcd0834fe981',1,'UI_front_part2.ser()'],['../UI__front__part3_8py.html#a2372f4434a994218c0b246f9c72597f1',1,'UI_front_part3.ser()'],['../UI__front__part4_8py.html#a281ed6008b954be935e3e986ff1dd806',1,'UI_front_part4.ser()']]],
  ['set_5fduty_90',['set_duty',['../classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['set_5fki_91',['set_Ki',['../classclosedLoop__part4_1_1closedLoop__part4.html#a17ceca1cb9c4bfed8f59182be7a80d1b',1,'closedLoop_part4::closedLoop_part4']]],
  ['set_5fkp_92',['set_Kp',['../classclosedLoop__part3_1_1closedLoop__part3.html#a02bc034cc703750e2d06e6e5edd3a072',1,'closedLoop_part3.closedLoop_part3.set_Kp()'],['../classclosedLoop__part4_1_1closedLoop__part4.html#a1fe1807826adb67cd1d211f8e6da363c',1,'closedLoop_part4.closedLoop_part4.set_Kp()']]],
  ['set_5fposition_93',['set_position',['../classencoderDriver__part2_1_1encoderDriver__part2.html#ae8681c9d4d8c1a69c7612e3ff988f3ec',1,'encoderDriver_part2.encoderDriver_part2.set_position()'],['../classencoderDriver__part3_1_1encoderDriver__part3.html#a1f0379fd4ca9921d4e48ff0eaa21facb',1,'encoderDriver_part3.encoderDriver_part3.set_position()'],['../classencoderDriver__part4_1_1encoderDriver__part4.html#a98a0c97098183eb2ce23c5332dd1fa2b',1,'encoderDriver_part4.encoderDriver_part4.set_position()']]],
  ['shares_2epy_94',['shares.py',['../shares_8py.html',1,'']]],
  ['speedmeas_95',['speedMeas',['../classencoderDriver__part3_1_1encoderDriver__part3.html#a26ad481d60b5bf66c248dd999dff7dee',1,'encoderDriver_part3.encoderDriver_part3.speedMeas()'],['../classencoderDriver__part4_1_1encoderDriver__part4.html#abd2fd63f984b8e381fbe0d2aaa655a08',1,'encoderDriver_part4.encoderDriver_part4.speedMeas()']]],
  ['speedref_96',['speedref',['../UI__front__part4_8py.html#a33b1a5fba7e4a955545e9228bdaba19b',1,'UI_front_part4']]],
  ['state_97',['state',['../UI__backTask__part1_8py.html#a91b864faefb913128a2eb58e239cd632',1,'UI_backTask_part1']]]
];
