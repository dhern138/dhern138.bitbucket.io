var searchData=
[
  ['set_5fduty_170',['set_duty',['../classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['set_5fki_171',['set_Ki',['../classclosedLoop__part4_1_1closedLoop__part4.html#a17ceca1cb9c4bfed8f59182be7a80d1b',1,'closedLoop_part4::closedLoop_part4']]],
  ['set_5fkp_172',['set_Kp',['../classclosedLoop__part3_1_1closedLoop__part3.html#a02bc034cc703750e2d06e6e5edd3a072',1,'closedLoop_part3.closedLoop_part3.set_Kp()'],['../classclosedLoop__part4_1_1closedLoop__part4.html#a1fe1807826adb67cd1d211f8e6da363c',1,'closedLoop_part4.closedLoop_part4.set_Kp()']]],
  ['set_5fposition_173',['set_position',['../classencoderDriver__part2_1_1encoderDriver__part2.html#ae8681c9d4d8c1a69c7612e3ff988f3ec',1,'encoderDriver_part2.encoderDriver_part2.set_position()'],['../classencoderDriver__part3_1_1encoderDriver__part3.html#a1f0379fd4ca9921d4e48ff0eaa21facb',1,'encoderDriver_part3.encoderDriver_part3.set_position()'],['../classencoderDriver__part4_1_1encoderDriver__part4.html#a98a0c97098183eb2ce23c5332dd1fa2b',1,'encoderDriver_part4.encoderDriver_part4.set_position()']]]
];
