/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Operation Manual", "index.html", "index" ],
    [ "Week 1", "page1.html", [
      [ "Part 1: Extending Interface", "page1.html#sec", [
        [ "The first UI_front_part1.py", "page1.html#subsection1", null ],
        [ "The second UI_backTask_part1.py", "page1.html#subsection2", null ]
      ] ]
    ] ],
    [ "Week 2", "page2.html", [
      [ "Part 2: Incremental Encoders", "page2.html#sec2", [
        [ "The first UI_front_part2.py", "page2.html#subsection3", null ],
        [ "The second UI_backTask_part2.py", "page2.html#subsection4", null ],
        [ "The third encoderTask.py", "page2.html#subsection5", null ],
        [ "The fourth encoderDriver_part2.py", "page2.html#subsection6", null ],
        [ "The fifth shares.py", "page2.html#subsection7", null ],
        [ "The sixth main_part2.py", "page2.html#subsection8", null ]
      ] ]
    ] ],
    [ "Week 3", "page3.html", [
      [ "Part 3: Motor Control", "page3.html#sec3", [
        [ "The first UI_front_part3.py", "page3.html#subsection9", null ],
        [ "The second UI_backTask_part3.py", "page3.html#subsection10", null ],
        [ "The third controllerTask_part3.py", "page3.html#subsection11", null ],
        [ "The fourth encoderDriver_part3.py", "page3.html#subsection12", null ],
        [ "The fifth shares.py", "page3.html#subsection13", null ],
        [ "The sixth main_part3.py", "page3.html#subsection14", null ],
        [ "The seventh MotorDriver.py", "page3.html#subsection15", null ],
        [ "The eighth closedLoop_part3.py", "page3.html#subsection16", null ]
      ] ]
    ] ],
    [ "Week 4", "page4.html", [
      [ "Part 4: Reference Tracking", "page4.html#sec4", [
        [ "The first UI_front_part4.py", "page4.html#subsection17", null ],
        [ "The second UI_backTask_part4.py", "page4.html#subsection18", null ],
        [ "The third controllerTask_part4.py", "page4.html#subsection19", null ],
        [ "The fourth encoderDriver_part4.py", "page4.html#subsection20", null ],
        [ "The fifth shares.py", "page4.html#subsection21", null ],
        [ "The sixth main_part4.py", "page4.html#subsection22", null ],
        [ "The seventh MotorDriver.py", "page4.html#subsection23", null ],
        [ "The eighth closedLoop_part4.py", "page4.html#subsection24", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"MotorDriver_8py.html",
"classencoderDriver__part3_1_1encoderDriver__part3.html#afd423fda77beee1dc2ff07da753bffee"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';