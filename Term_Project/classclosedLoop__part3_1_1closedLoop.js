var classclosedLoop__part3_1_1closedLoop =
[
    [ "__init__", "classclosedLoop__part3_1_1closedLoop.html#add4dc8f9f0b50f191d12964dc26d04e8", null ],
    [ "get_Kp", "classclosedLoop__part3_1_1closedLoop.html#a9dea115a38c1fa047b13619d6d73bc18", null ],
    [ "set_Ki", "classclosedLoop__part3_1_1closedLoop.html#ad71bdd99ee90a8df384f83b799b24f3f", null ],
    [ "set_Kp", "classclosedLoop__part3_1_1closedLoop.html#aec3ef179dc61173850d2d2717570a5d9", null ],
    [ "update", "classclosedLoop__part3_1_1closedLoop.html#a6b4f985437967c68103a5e2be4a0ac5a", null ],
    [ "Ki", "classclosedLoop__part3_1_1closedLoop.html#ad0c3b85c3c65086aa7cb2894aa91aa96", null ],
    [ "Kp", "classclosedLoop__part3_1_1closedLoop.html#ade783eb9bb87e515831ddebec20b6f53", null ],
    [ "Pmax", "classclosedLoop__part3_1_1closedLoop.html#adc5b25544019d4e335fdf84db3d7774f", null ],
    [ "Pmin", "classclosedLoop__part3_1_1closedLoop.html#a752955f9c4b9ff451fa61bbb60980b44", null ],
    [ "PWM", "classclosedLoop__part3_1_1closedLoop.html#a71dd33e30714e39be0cc7cb264d7ffff", null ],
    [ "PWMdelta", "classclosedLoop__part3_1_1closedLoop.html#a25fa7b0f937e505987ab92a6e12916a5", null ]
];