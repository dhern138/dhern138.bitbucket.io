var UI__front__part4_8py =
[
    [ "kb_cb", "UI__front__part4_8py.html#af6c34fa4d139d7f34b6fb76c2d9e043d", null ],
    [ "write_csv", "UI__front__part4_8py.html#a4235b0889d2eec4d5610e2bbfe3e3bff", null ],
    [ "axs", "UI__front__part4_8py.html#ae1ff79707a958f3cefced0a948a392f0", null ],
    [ "c", "UI__front__part4_8py.html#a9e68df61a82cbc997c650edfa9e4a299", null ],
    [ "callback", "UI__front__part4_8py.html#a75b3d942d1f35d77b17663df5fe22893", null ],
    [ "dataCollect", "UI__front__part4_8py.html#a10a0ceea80d305f454486f20debe2f01", null ],
    [ "dataSplit", "UI__front__part4_8py.html#a8c9626179697232e4f23d0210d189a01", null ],
    [ "dataStrip", "UI__front__part4_8py.html#a431bdfd5a0d5e201a76ccb7378086814", null ],
    [ "f", "UI__front__part4_8py.html#ada2dcaf30b08ff18ff1112bb7d73ed62", null ],
    [ "fig", "UI__front__part4_8py.html#ae55fed163b926e773a3d5a69efe2646f", null ],
    [ "last_key", "UI__front__part4_8py.html#ac8ccc843dc14d89c7388b2572b910b79", null ],
    [ "n", "UI__front__part4_8py.html#a333011165c8cf58ce5fd0a8390a90fc9", null ],
    [ "p", "UI__front__part4_8py.html#aee82e5b708bf15a974d21efff6ffa9bd", null ],
    [ "position", "UI__front__part4_8py.html#ae0de61bb9cbfb01715e21da424d7aa4c", null ],
    [ "s", "UI__front__part4_8py.html#a70f3c9ff9b52ecd344baf6d831592b18", null ],
    [ "ser", "UI__front__part4_8py.html#a281ed6008b954be935e3e986ff1dd806", null ],
    [ "speedref", "UI__front__part4_8py.html#a33b1a5fba7e4a955545e9228bdaba19b", null ],
    [ "t", "UI__front__part4_8py.html#a87e5e612a4b718f323f4b315b7cf2480", null ],
    [ "times", "UI__front__part4_8py.html#af2bc1efc51dfaa28dbd7f050124a98c0", null ],
    [ "value", "UI__front__part4_8py.html#a5df17e4688f6f0094220470dda7569bf", null ],
    [ "values", "UI__front__part4_8py.html#a850be8531ec49ab66082f47d7059c0e6", null ]
];