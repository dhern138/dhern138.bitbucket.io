var classclosedLoop__part4_1_1closedLoop__part4 =
[
    [ "__init__", "classclosedLoop__part4_1_1closedLoop__part4.html#a429c53bd4b511406186aadf60f5007ac", null ],
    [ "get_Kp", "classclosedLoop__part4_1_1closedLoop__part4.html#a9323894598f7edc5643d0784a3cc0fe7", null ],
    [ "set_Ki", "classclosedLoop__part4_1_1closedLoop__part4.html#a17ceca1cb9c4bfed8f59182be7a80d1b", null ],
    [ "set_Kp", "classclosedLoop__part4_1_1closedLoop__part4.html#a1fe1807826adb67cd1d211f8e6da363c", null ],
    [ "update", "classclosedLoop__part4_1_1closedLoop__part4.html#ace7e40eb34bb2a1cc6e53fcddded8c91", null ],
    [ "Ki", "classclosedLoop__part4_1_1closedLoop__part4.html#ad6efcd6ec306da77cad3cd7e292cc076", null ],
    [ "Kp", "classclosedLoop__part4_1_1closedLoop__part4.html#a82b80dc842ab488977fac050cf485ba4", null ],
    [ "Pmax", "classclosedLoop__part4_1_1closedLoop__part4.html#ae7973aec2683d0cd55ba7ff079359b91", null ],
    [ "Pmin", "classclosedLoop__part4_1_1closedLoop__part4.html#aefec5f7bad793d15b4f84170b67f009d", null ],
    [ "PWM", "classclosedLoop__part4_1_1closedLoop__part4.html#a9e1d5ada22eff6ee5bf0a4315ea4cb22", null ],
    [ "PWMdelta", "classclosedLoop__part4_1_1closedLoop__part4.html#add4642de727d77d438c41fa1545f0db4", null ]
];